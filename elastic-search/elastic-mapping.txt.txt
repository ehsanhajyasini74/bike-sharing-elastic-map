PUT bikes
{
    "mappings": {
        "bike": {
            "properties": {
                "owner": {"type": "text"},
                "location": {"type": "geo_point"}
            }
        }
    }
}