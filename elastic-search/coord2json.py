index_data='{ "index":{} }'
names = ['Ehsan', 'Mark', 'Frank', 'BinBin', 'Vovi', 'DV', 'Raffi', 'Harsh', 'Michi', 'Dimitri', 'James']
i = 0
def get_name():
	global i
	i = (i+1) % len(names)
	return names[i]

data = ""
with open("bike_coord.txt") as f:
	for l in f:
		coord = l.strip().split(',')
		bike = '{"owner": "' + get_name() + '\'s bike", "location": {"lat": "'+coord[1]+'", "lon": "' + coord[0] + '"}}';
		data += index_data+ '\n' + bike + '\n'; 
with open("bike_data.json", 'w') as w:
	w.write(data)

