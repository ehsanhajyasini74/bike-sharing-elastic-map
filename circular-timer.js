import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
} from 'react-native';

import {styles} from './styles.js';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import reactMixin from 'react-mixin';
import TimerMixin from 'react-timer-mixin';

/* Usage Props
 *  @time: total time in seconds
 *  @timerRunning: boolean, status of timer
 */
export default class CircularTimer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fill: 100,
            time: props.time
        };
    }

    runTimer(props = this.props) {
        if(this.state.time <= 0 || props.timerRunning === false)
            return;
        this.setState({time: this.state.time-1});

        this.setTimeout(() => {
            let remainingFill = Math.floor(this.state.time / props.time * 100);
            console.warn("remaining fill: " + remainingFill);
            this.setState({fill: remainingFill});
            this.runTimer();
        }, 1000); //every second
    }

    componentDidMount() {
        this.runTimer();
    }
    componentWillReceiveProps(newProps) {
        if(this.props.timerRunning === false && newProps.timerRunning === true)
            this.runTimer(newProps);
    }

    render() {
        return <AnimatedCircularProgress
                size={200}
                width={10}
                prefill={100}
                fill={this.state.fill}
                tintColor="#00e0ff"
                backgroundColor="#3d5875">
                {
                    (fill) => (
                        <Text style={styles.points}>
                            { Math.floor(this.state.time/60)} : {this.state.time % 60}
                        </Text>
                    )
                }
            </AnimatedCircularProgress>
    }
}

reactMixin(CircularTimer.prototype, TimerMixin);
