// elastic search
export const url = 'https://edccf30544631f612e90c7f4f771df1a.us-east-1.aws.found.io:9243';
export const authHeader = 'Basic ' + 'ZWxhc3RpYzpLSHY1Nk9tWWIwZEFobGxEY3NrMFp0Ymg=';
export const index = 'bikes';
export const type = 'bike';

//map
export const initialRegion =  {
        latitude: 48.17,
        longitude: 11.59,
        latitudeDelta: 0.0022,
        longitudeDelta: 0.0022,
};