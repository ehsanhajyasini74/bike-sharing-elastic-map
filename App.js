/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
    Button,
    TouchableOpacity,
    Image,
} from 'react-native';

import {styles} from './styles.js';
import PictureConfirmationView from './picture-confirmation';
import CircularTimer from "./circular-timer";
import ImagePicker from 'react-native-image-picker';

export default class MyApp extends React.Component {
    state = {
    };

    render() {
        // return <SearchMap searchCount={2} searchRadius={1}/>;
        return <PictureConfirmationView url={'https://www.floydlabs.com/expose/BxTdVWndGa63gXbQ5x7fna'}/>
    }
}
