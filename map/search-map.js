/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import MapView, {Marker} from 'react-native-maps';
import ElasticSearch from './elastic';
import {styles} from './styles';
import {url, authHeader, index, type, initialRegion} from '../constants';


/* Usage Props
 * @searchRadius: area to search in 'km'
 * @searchCount: number of expected results
 */
export default class SearchMap extends React.Component {
    state = {
        bikes: [],
        region: initialRegion
    };

    fetchRaw = (lat, lon) => {
        fetch(this.elasticSearch.getElasticSearchUrl(index, type), {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': authHeader
            },
            body: this.elasticSearch.getElasticLocationQuery(lat, lon, this.props.searchRadius, this.props.searchCount),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({bikes:responseJson.hits.hits});
                console.warn(this.state.bikes);
            })
            .catch((error) => {
                console.error(error);
            });
    };


    constructor(props){
        // Pass props to parent class
        super(props);
        this.elasticSearch = new ElasticSearch(url, authHeader);
    }

    onRegionChangeComplete(region) {
        this.setState({ region });
        this.fetchRaw(region.latitude, region.longitude);
        console.warn("New Region " + region.latitude + ' / ' + region.longitude);
    };

    render() {
        const { region } = this.props;
        console.log(region);
        return (
            <MapView
                style={styles.map}
                initialRegion={this.state.region}
                onRegionChangeComplete={this.onRegionChangeComplete.bind(this)}
            >
                {(this.state.bikes) ?
                    this.state.bikes.map((city) =>
                        (<Marker
                            key={city._id}
                            coordinate={{
                                latitude: parseFloat(city._source.location.lat),
                                longitude: parseFloat(city._source.location.lon)
                            }}
                            title={city._source.owner}
                        /> ))
                    : null}
            </MapView>
        );
    }
}