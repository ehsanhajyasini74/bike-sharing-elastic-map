import {StyleSheet,PixelRatio} from "react-native";

export const styles = StyleSheet.create({
    // container: {
    //     ...StyleSheet.absoluteFillObject,
    //     height: 400,
    //     width: 400,
    //     justifyContent: 'flex-end',
    //     alignItems: 'center',
    // },
    points: {
        backgroundColor: 'transparent',
        position: 'absolute',
        top: 70,
        left: 44,
        width: 90,
        textAlign: 'center',
        color: '#7591af',
        fontSize: 30,
        fontWeight: "100"
    },
    Imagecontainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    avatarContainer: {
        borderColor: '#9B9B9B',
        borderWidth: 1 / PixelRatio.get(),
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatar: {
        // borderRadius: 75,
        width: 400,
        height: 400
    }
});