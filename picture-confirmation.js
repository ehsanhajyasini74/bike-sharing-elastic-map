import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    TouchableOpacity,
    Image,
} from 'react-native';
import ImagePicker from "react-native-image-picker";
import {styles} from "./styles";

const max_size = 500;

var options = {
    title: 'Take a photo of the bike',
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};

/* Usage Props
 * @url: url to tensorflow model endpoint
 */
export default class PictureConfirmationView extends React.Component {
    state = {
        pictureSource: null,
        bike: null,
        probability: 0
    };

    upload_picture(response) {
        server_url = this.props.url; //'https://www.floydlabs.com/expose/BxTdVWndGa63gXbQ5x7fna';
        let form_data = new FormData();
        form_data.append('file', {
            uri: response.uri,
            type: response.type, // or photo.type
            name: response.fileName
        });

        fetch(server_url,{
            method: 'POST',
            headers: {
                Accept: '*',
                'Content-Type': 'multipart/form-data',
            },
            body: form_data
        }).then((res) => res.json())
            .then((responseJson) => {
                console.warn(responseJson);
                let bike_name = Object.keys(responseJson)[0];
                this.setState({'bike': bike_name,
                    'probability': responseJson[bike_name]});
                console.warn(this.state);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: max_size,
            maxHeight: max_size,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.launchCamera(options, (response) => {
            console.warn('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else {
                let source = { uri: response.uri };
                this.upload_picture(response);
                this.setState({
                    pictureSource: source
                });
            }
        });
    }

    render() {
        return <View style={styles.Imagecontainer}>
            <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
                    { this.state.pictureSource === null ? <Text> Select a Photo</Text> :
                        <Image style={styles.avatar} source={this.state.pictureSource} />

                    }
                </View>
                {this.state.bike === null ? <Text> Nothing Detected! </Text> :
                    <Text> Detected [{this.state.bike}] with a probability of [{this.state.probability}]</Text>
                }
            </TouchableOpacity>
        </View>
    }
}
