export default class ElasticSearch {
    constructor(url, authHeader) {
        this.url = url;
        this.authHeader = authHeader;
    }

    getElasticSearchUrl(index, type) {
        return this.url + '/' + index + '/' + type + '/_search';
    }

    getElasticLocationQuery(lat,lon, radius, size) {
        let searchRadius = radius + "km";
        return JSON.stringify({
            "size" : size,
            "sort": [
                {
                    "_geo_distance": {
                        "location": {
                            "lat": lat,
                            "lon": lon
                        },
                        "order": "asc",
                        "unit": "km"
                    }
                }
            ],
            "query": {
                "bool": {
                    "must": {
                        "match_all": {}
                    },
                    "filter": {
                        "geo_distance": {
                            "distance": searchRadius,
                            "location": {
                                "lat": lat,
                                "lon": lon
                            }
                        }
                    }
                }
            }
        })
    }
}